package com.example.artistalbumapi.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.example.artistalbumapi.models.Artist;
import com.example.artistalbumapi.services.ArtistService;

@RestController
public class ArtistController {
    @Autowired
    ArtistService artistService;

    @GetMapping("/artists/{index}")
    public Artist getIndexReult(@PathVariable int index) {
        return artistService.getArtistByIndex(index);
    }
}
