package com.example.arrayfilterrestapi.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.arrayfilterrestapi.services.RainbowService;

@RestController
public class RainbowController {
    @Autowired
    RainbowService rainbowService;

    @GetMapping("/array-int-request-query")
    public ArrayList<Integer> getNumPos(@RequestParam int pos) {
        return rainbowService.fillterNumber(pos);
    }

    @GetMapping("/array-int-param/{index}")
    public Integer getIndex(@PathVariable int index) {
        return rainbowService.getNumByIndex(index);
    }
}
