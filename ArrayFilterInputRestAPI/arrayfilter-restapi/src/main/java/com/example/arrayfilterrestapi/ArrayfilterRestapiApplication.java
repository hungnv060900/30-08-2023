package com.example.arrayfilterrestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArrayfilterRestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArrayfilterRestapiApplication.class, args);
	}

}
