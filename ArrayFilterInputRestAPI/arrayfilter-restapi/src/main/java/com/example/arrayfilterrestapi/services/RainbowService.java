package com.example.arrayfilterrestapi.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

@Service
public class RainbowService {
    private int[] rainbows = { 1, 23, 32, 43, 54, 65, 86, 10, 15, 16, 18 };

    public ArrayList<Integer> fillterNumber(int pos) {
        ArrayList<Integer> nuList = new ArrayList<>();
        for (int num : rainbows) {
            if (num > pos) {
                nuList.add(num);
            }
        }
        return nuList;
    }

    public Integer getNumByIndex(int index) {
        // int rainbows=0;
        if (index >= 0 && index < rainbows.length) {
            return rainbows[index];
        } else {
            return null;
        }
    }
}
