package com.example.countryregionapi.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.example.countryregionapi.models.Region;



@Service
public class RegionService {
    private Region haNoi = new Region("HN", "Hà Nội");
    private Region daNang = new Region("DN", "Đà Nẵng");
    private Region tpHoChiMinh = new Region("TPHCM", "Thành phố Hồ Chí Minh");

    private Region tokyo = new Region("TKO", "Tokyo");
    private Region chiba = new Region("CB", "Chiba");
    private Region osaka = new Region("OSK", "Osaka");

    private Region newyork = new Region("NY", "New York");
    private Region florida = new Region("FL", "Florida");
    private Region miami = new Region("MA", "Miami");

    public ArrayList<Region> getVietNamRegions() {
        ArrayList<Region> regions = new ArrayList<>();

        regions.add(haNoi);
        regions.add(daNang);
        regions.add(tpHoChiMinh);

        return regions;
    }

    public ArrayList<Region> getJapanRegions() {
        ArrayList<Region> regions = new ArrayList<>();

        regions.add(tokyo);
        regions.add(chiba);
        regions.add(osaka);

        return regions;
    }

    public ArrayList<Region> getUSARegions() {
        ArrayList<Region> regions = new ArrayList<>();

        regions.add(newyork);
        regions.add(florida);
        regions.add(miami);

        return regions;
    }

    public Region getRegion(String regionCode) {
        ArrayList<Region> regions = new ArrayList<>();

        regions.add(haNoi);
        regions.add(daNang);
        regions.add(tpHoChiMinh);
        regions.add(tokyo);
        regions.add(chiba);
        regions.add(osaka);
        regions.add(newyork);
        regions.add(florida);
        regions.add(miami);

        for (Region region : regions) {
            if(region.getRegionCode().equals(regionCode)) {
                return region;
            }
        }

        return null;
    }
}
