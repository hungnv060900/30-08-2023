package com.example.countryregionapi.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.example.countryregionapi.models.Country;
import com.example.countryregionapi.services.CountryService;

@RestController
public class CountryController {
    @Autowired
    CountryService countryService;

    @GetMapping("/countries/{index}")
    public Country result (@PathVariable int index){
        return countryService.getIndexByCountry(index);
    }
}
