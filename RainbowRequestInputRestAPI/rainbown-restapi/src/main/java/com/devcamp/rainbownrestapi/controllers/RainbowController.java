package com.devcamp.rainbownrestapi.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.rainbownrestapi.services.RainbowService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class RainbowController {
    @Autowired
    RainbowService rainbowService;

    @GetMapping("/rainbow-request-query")
    public ArrayList<String> searchRainbow(@RequestParam String keyword) {
        ArrayList<String> result = rainbowService.searchRainbows(keyword);
        return result;
    }

    @GetMapping("/rainbow-request-param/{index}")
    public String getIndexRainbow(@PathVariable int index) {
        return rainbowService.getIndexRainbow(index);
    }
}
