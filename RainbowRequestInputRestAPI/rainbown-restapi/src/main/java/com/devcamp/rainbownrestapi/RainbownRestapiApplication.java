package com.devcamp.rainbownrestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RainbownRestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RainbownRestapiApplication.class, args);
	}

}
